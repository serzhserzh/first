$(document).ready(function () {
    $('input').iCheck({
        radioClass: 'iradio_flat-blue'
    });

    $('input').on('ifChecked', function (event) {
        $('.continue').removeAttr('disabled');
    });

    $(".close-modal-hidden").on('click', function () {
        $("#sum").val('');
    });

    $("input#sum").on('focus', function () {
        $('div.hidden-reset').removeClass('hidden-reset').addClass('active');
    });

    $('#my-modal').on('click', function (event) {
       if (!$(event.target).closest('input#sum')){
           $('div.hidden-reset').hide();
       }
    });

    $('#modal-btn').on('click', function () {
        $('#my-modal').show();
    });

    $('.close-modal').on('click', function () {
        $('#my-modal').hide();
    });

    $('.cancel').on('click', function () {
        $('#my-modal').hide();
    });

/*    $('body').on('click',function(event){
        if (!$(event.target).closest('#my-modal').length){
            $('#my-modal').hide();
        }
    });*/

    $('input#sum').keydown(function (event) {
        if (event.keyCode == 46 || event.keyCode == 8 || (event.keyCode >= 35 && event.keyCode <=39)){
            return;
        }
        else{
            if ((event.keyCode < 48 || event.keyCode > 57)&&(event.keyCode < 96 || event.keyCode > 105)){
                event.preventDefault();
            }
        }
    });

});




